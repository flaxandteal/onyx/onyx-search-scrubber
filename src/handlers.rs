use crate::{Area, ScrubberDB};
use axum::extract::{Extension, Query};
use axum::Json;
use fst::{Automaton, IntoStreamer, Streamer};
use schemars::schema_for;
use schemars::JsonSchema;
use serde::{Deserialize, Serialize};
use std::sync::Arc;
use std::time::Instant;

pub async fn health_check_handler() -> &'static str {
    "OK"
}

pub async fn scrubber_schema_handler() -> String {
    let schema = schema_for!(ScrubberResp);
    serde_json::to_string(&schema).expect("json schema")
}

#[derive(Debug, Deserialize)]
pub struct ScrubberParams {
    q: String,
}

#[derive(Serialize, JsonSchema)]
pub struct ScrubberResp {
    time: String,
    query: String,
    results: Results,
}

#[derive(Serialize, JsonSchema)]
pub struct Results {
    areas: Vec<AreaResp>,
    industries: Vec<IndustryResp>,
}

#[derive(Serialize, JsonSchema)]
pub struct AreaResp {
    name: &'static str,
    region: &'static str,
    region_code: &'static str,
    codes: Vec<&'static str>,
}
impl AreaResp {
    fn from_area(a: &Area) -> Self {
        AreaResp {
            name: a.la_name.as_str(),
            region: a.region_name.as_str(),
            region_code: a.region_code.as_str(),
            codes: vec![a.oa_code.as_str()],
        }
    }
}

#[derive(Serialize, JsonSchema)]
pub struct IndustryResp {
    code: &'static str,
    name: &'static str,
}

pub async fn prefix_search_handler(
    Query(params): Query<ScrubberParams>,
    Extension(db): Extension<Arc<ScrubberDB>>,
) -> Json<ScrubberResp> {
    let start_time = Instant::now();
    let lower = params.q.to_lowercase();
    let query = lower.split(' ').collect::<Vec<_>>();
    let industries = query
        .iter()
        .flat_map(|w| industry_prefix_search(&db, w))
        .collect();
    let areas = query
        .iter()
        .flat_map(|w| area_prefix_search(&db, w))
        .collect();
    Json(ScrubberResp {
        time: format!("{:.3?}", start_time.elapsed()),
        query: params.q,
        results: Results { areas, industries },
    })
}

pub fn industry_prefix_search(db: &ScrubberDB, query: &str) -> Vec<IndustryResp> {
    if query.len() > 2 {
        let prefix_matcher = fst::automaton::Str::new(&query).starts_with();
        let mut stream = db.industries_fst.search(prefix_matcher).into_stream();
        let mut industries: Vec<IndustryResp> = vec![];
        while let Some((_, v)) = stream.next() {
            let industry = db.industries.get(v as usize).unwrap();
            industries.push(IndustryResp {
                code: industry.code.as_str(),
                name: industry.name.as_str(),
            })
        }
        industries
    } else {
        vec![]
    }
}

fn area_prefix_search(db: &ScrubberDB, query: &str) -> Vec<AreaResp> {
    if query.len() > 3 {
        let prefix_matcher = fst::automaton::Str::new(&query).starts_with();
        let mut stream = db.areas_fst.search(&prefix_matcher).into_stream();
        let mut areas: Vec<AreaResp> = vec![];
        while let Some((_k, v)) = stream.next() {
            if areas.len() > 16 {
                break;
            }
            let area = db.areas.get(v as usize).unwrap();
            match areas.last_mut() {
                None => areas.push(AreaResp::from_area(area)),
                Some(old) => {
                    match old.name == area.la_name.as_str()
                        && old.region == area.region_name.as_str()
                    {
                        true => old.codes.push(area.oa_code.as_str()),
                        false => areas.push(AreaResp::from_area(area)),
                    }
                }
            }
        }
        areas
    } else {
        vec![]
    }
}
