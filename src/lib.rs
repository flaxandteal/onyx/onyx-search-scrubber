pub mod data_loader;
pub mod handlers;

use tracing::level_filters::LevelFilter;
use tracing::subscriber::set_global_default;
use tracing::Subscriber;
use tracing_log::LogTracer;
use ustr::Ustr;

/// Register a subscriber as global default to process span data.
/// It should only be called once!
pub fn init_subscriber(subscriber: impl Subscriber + Send + Sync) {
    LogTracer::init().expect("Failed to set logger");
    set_global_default(subscriber).expect("Failed to set subscriber");
}

pub fn init_logging(log_level: tracing::Level) {
    let subscriber = tracing_subscriber::fmt()
        .with_thread_names(true)
        .with_max_level(LevelFilter::from_level(log_level))
        .finish();
    init_subscriber(subscriber);
}

#[derive(Debug, Clone)]
pub struct Area {
    pub oa_code: Ustr,
    pub la_code: Ustr,
    pub la_name: Ustr,
    pub region_name: Ustr,
    pub region_code: Ustr,
}

#[derive(Debug, Clone)]
pub struct Industry {
    code: Ustr,
    name: Ustr,
}

pub struct ScrubberDB {
    pub areas: Vec<Area>,
    pub areas_fst: fst::Map<Vec<u8>>,
    pub industries: Vec<Industry>,
    pub industries_fst: fst::Map<Vec<u8>>,
}
