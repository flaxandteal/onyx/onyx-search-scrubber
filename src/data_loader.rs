use crate::{Area, Industry, ScrubberDB};
use csv::{Reader, ReaderBuilder};
use serde::Deserialize;
use std::env;
use std::fs::File;
use tokio::time::Instant;
use tracing::info;

#[derive(Debug, Deserialize)]
struct AreaRaw {
    #[serde(rename = "Output Area Code")]
    output_area_code: String,
    #[serde(rename = "Local Authority Code")]
    local_authority_code: String,
    #[serde(rename = "Local Authority Name")]
    la_name: String,
    #[serde(rename = "Region/Country Code")]
    region_code: String,
    #[serde(rename = "Region/Country Name")]
    region_name: String,
}
impl AreaRaw {
    fn to_area(self) -> Area {
        Area {
            oa_code: self.output_area_code.to_lowercase().into(),
            la_code: self.local_authority_code.to_lowercase().into(),
            la_name: self.la_name.to_lowercase().into(),
            region_name: self.region_name.to_lowercase().into(),
            region_code: self.region_code.to_lowercase().into(),
        }
    }
}

#[derive(Debug, Deserialize)]
struct IndustryRaw {
    #[serde(rename = "SIC Code")]
    code: String,
    #[serde(rename = "Description")]
    name: String,
}
impl IndustryRaw {
    fn to_industry(self) -> Industry {
        Industry {
            code: self.code.to_lowercase().into(),
            name: self.name.to_lowercase().into(),
        }
    }
}

pub fn load_csv_data() -> csv::Result<ScrubberDB> {
    let init = Instant::now();
    let mut areas_rdr = read_csv_file("2011 OAC Clusters and Names csv v2.csv");
    let mut areas = areas_rdr
        .deserialize::<AreaRaw>()
        .map(|r| r.map(|r| r.to_area()).expect("CSV decode"))
        .collect::<Vec<_>>();
    areas.sort_unstable_by(|a, b| a.oa_code.as_str().cmp(b.oa_code.as_str()));
    let areas_fst = fst::Map::from_iter(
        areas
            .iter()
            .enumerate()
            .map(|(i, area)| (area.oa_code.as_str(), i as u64)),
    )
    .expect("FST build");
    let mut industr_rdr = read_csv_file("SIC07_CH_condensed_list_en.csv");
    let mut industries = industr_rdr
        .deserialize::<IndustryRaw>()
        .map(|r| r.map(|r| r.to_industry()).expect("CSV decode"))
        .collect::<Vec<_>>();
    industries.sort_unstable_by(|a, b| a.code.as_str().cmp(b.code.as_str()));
    let industries_fst = fst::Map::from_iter(
        industries
            .iter()
            .enumerate()
            .map(|(i, ind)| (ind.code.as_str(), i as u64)),
    )
    .expect("FST build");
    info!(
        "CSVs read in {:.3?}. {} areas and {} industries",
        init.elapsed(),
        areas.len(),
        industries.len()
    );
    Ok(ScrubberDB {
        areas,
        areas_fst,
        industries,
        industries_fst,
    })
}

fn read_csv_file(file_name: &'static str) -> Reader<File> {
    let current_dir = env::current_dir().expect("get current directory");
    let data_dir = current_dir.join("data");
    let file_path = data_dir.join(file_name);
    let file = File::open(file_path).expect("Read CSV File");
    ReaderBuilder::new().from_reader(file)
}
