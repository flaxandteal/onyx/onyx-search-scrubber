use axum::routing::get;
use axum::{AddExtensionLayer, Router};
use onyx_search_scrubber::handlers::{prefix_search_handler, scrubber_schema_handler};
use onyx_search_scrubber::{data_loader, handlers, init_logging};
use std::sync::Arc;
use structopt::StructOpt;
use tower_http::trace::TraceLayer;
use tracing::info;

#[derive(StructOpt)]
struct CliArgs {
    #[structopt(long = "log-level", case_insensitive = true, default_value = "INFO")]
    log_level: tracing::Level,
}

#[tokio::main]
async fn main() {
    let args = CliArgs::from_args();
    init_logging(args.log_level);
    let db = data_loader::load_csv_data().expect("CSV Decode");
    let db = Arc::new(db);

    let app = Router::new()
        .route("/scrubber/search", get(prefix_search_handler))
        .route("/scrubber/json-schema", get(scrubber_schema_handler))
        .route("/health", get(handlers::health_check_handler))
        .layer(AddExtensionLayer::new(Arc::clone(&db)))
        .layer(TraceLayer::new_for_http());
    let addr = "0.0.0.0:3002";
    info!("Will listen on {addr}");
    axum::Server::bind(&addr.parse().unwrap())
        .serve(app.into_make_service())
        .await
        .unwrap();
}
